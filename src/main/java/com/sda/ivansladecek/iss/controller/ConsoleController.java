package com.sda.ivansladecek.iss.controller;

import com.sda.ivansladecek.iss.dto.PersonDTO;
import com.sda.ivansladecek.iss.dto.Position;
import com.sda.ivansladecek.iss.model.IssSpeed;
import com.sda.ivansladecek.iss.service.IssService;
import com.sda.ivansladecek.iss.service.PersonService;
import com.sda.ivansladecek.iss.service.PositionService;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ConsoleController {

    private final IssService issService;
    private final PositionService positionService;
    private final PersonService personService;

    public ConsoleController(IssService issService, PositionService positionService, PersonService personService) {
        this.issService = issService;
        this.positionService = positionService;
        this.personService = personService;
    }

    private static final Logger LOGGER = LoggerFactory.getLogger(ConsoleController.class);

    public Position getCurrentISSPosition(){
        LOGGER.info("\nShowing current ISS position...\n");
       var issPosition = issService.getIssPosition();

       positionService.create(issPosition);
       return issPosition;
    }

    public IssSpeed getISSSpeed() {
        LOGGER.info("\nGet current ISS speed...\n");
        var issPosition1 = issService.getIssPosition();

        try {
            Thread.sleep(3000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        var issPosition2 = issService.getIssPosition();
        return positionService.storeSpeed(issPosition1, issPosition2);
    }

    public List<PersonDTO> getListOfPeople() {
        LOGGER.info("\nShowing Astronauts...\n");
        List<PersonDTO> astronauts = issService.getAstronauts();

        personService.create(astronauts);
        return astronauts;

    }
}

