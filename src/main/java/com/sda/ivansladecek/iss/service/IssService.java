package com.sda.ivansladecek.iss.service;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.ivansladecek.iss.client.IssClient;
import com.sda.ivansladecek.iss.dto.People;
import com.sda.ivansladecek.iss.dto.PersonDTO;
import com.sda.ivansladecek.iss.dto.Position;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;

// @Service
public class IssService {

    private static final Logger LOGGER = LoggerFactory.getLogger(IssService.class);

    private static final String ASTROS_URI = "http://api.open-notify.org/astros.json";
    private static final String ISS_POSITION_URI = "http://api.open-notify.org/iss-now.json";

    private final IssClient issClient;
    private final ObjectMapper objectMapper;


    // @Autowired
    public IssService(IssClient issClient, ObjectMapper objectMapper) {
        this.issClient = issClient;
        this.objectMapper = objectMapper;
    }

    public List<PersonDTO> getAstronauts() {
        String json = issClient.get(ASTROS_URI);

        return deserialize(json);
    }

    private List<PersonDTO> deserialize(String json) {
        try {
            People people = objectMapper.readValue(json, new TypeReference<People>() {
            });

            return people.getPeople();
        } catch (JsonProcessingException ex) {
            LOGGER.error("Failed to deserialize", ex);
            return List.of();
        }
        // TODO: getIssPosition, deserializePosition

    }

    public Position getIssPosition() {
        String json = issClient.get(ISS_POSITION_URI);

        return deserializePosition(json);
    }

    private Position deserializePosition(String json) {
        try {
            Position position = objectMapper.readValue(json, new TypeReference<Position>() {
            });

            return position;
        } catch (JsonProcessingException ex) {
            LOGGER.error("Failed to deserialize", ex);
            return null;
        }
    }
}




    //public String getPlace(String uri) {
       // return issClient.get(uri);


