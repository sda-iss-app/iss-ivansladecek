package com.sda.ivansladecek.iss.console;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.sda.ivansladecek.iss.client.IssClient;
import com.sda.ivansladecek.iss.controller.ConsoleController;
import com.sda.ivansladecek.iss.service.HibernateService;
import com.sda.ivansladecek.iss.service.IssService;
import com.sda.ivansladecek.iss.service.PersonService;
import com.sda.ivansladecek.iss.service.PositionService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class IssConsoleApplication {
    private static final Logger LOGGER = LoggerFactory.getLogger(IssConsoleApplication.class);

    private IssConsoleApplication() {
    }

    // simulate spring behavior
    public static void run() {
        var scanner = new Scanner();
        IssClient issClient = new IssClient();
        ObjectMapper objectMapper = new ObjectMapper();
        IssService issService = new IssService(issClient, objectMapper);
        HibernateService hibernateService = new HibernateService();

        PositionService positionService = new PositionService(hibernateService);
        PersonService personService = new PersonService(hibernateService);

        ConsoleController consoleController = new ConsoleController(issService, positionService, personService);

        while (true) {
            Menu.printMainMenu();
            int input = scanner.loadUserInput();

            if (input == 0) {
                break;
            }

            switch (input) {
                case 1:
                    LOGGER.info("{}", consoleController.getCurrentISSPosition());
                    break;
                case 2:
                    LOGGER.info("{}", consoleController.getISSSpeed());
                    break;
                case 3:
                    LOGGER.info("{}", consoleController.getListOfPeople());
                    break;
                default:
                    LOGGER.info("This option ({}) is not defined", input);
                    break;
            }
        }
    }
}
