package com.sda.ivansladecek.iss.dto;

import lombok.Data;

@Data
public class Cords {

    private Float longitude;
    private Float latitude;
}
