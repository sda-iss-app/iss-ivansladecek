package com.sda.ivansladecek.iss.dto;


import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor

public class People {
    private List<PersonDTO> people;
    private Integer number;
    private String message;

}
