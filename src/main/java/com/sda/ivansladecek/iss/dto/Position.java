package com.sda.ivansladecek.iss.dto;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor

    public class Position {

    private Long timestamp;
    private String message;
    @JsonProperty("iss_position")
    private Cords cords;

}

        //JsonProperty("name"), ak by sa volala napr. fullName
        /*private Integer measure_timestamp;
        private DecimalFormat latitude;
        private DecimalFormat longitude;
        private DateTimeFormatter measured_at;
        private Integer iss_speed_id;*/
